from bs4 import BeautifulSoup
import urllib
import unicodedata

def dump_data_to_csv(price_data,file_name):
    import csv
    with open(file_name, "w") as toWrite:
        writer = csv.writer(toWrite, delimiter=",")
        table_rows = price_data.findAll("tr")
        headerRow = table_rows[0]
        print "Writing headers"
        writer.writerow([header.get_text().encode('ascii','ignore') \
                            for header in headerRow.findAll('th')])
    
        print "Writing data"    
        for row_index in xrange(1,len(table_rows)): 
            writer.writerow([cell.get_text().encode('ascii','ignore') \
                            for cell in table_rows[row_index].findAll('td')])
    
def process(url, output_file_name):
    r = urllib.urlopen(url).read()
    soup = BeautifulSoup(r, 'lxml')
    tables = soup.findAll('table')
    price_table = tables[5]
    dump_data_to_csv(price_table, output_file_name)

daily_url='http://www.eia.gov/dnav/ng/hist/rngwhhdD.htm'
monthly_url='http://www.eia.gov/dnav/ng/hist/rngwhhdm.htm'
daily_file = './dailyOutput.csv'
monthly_file = './monthlyOutput.csv'

print "Processing daily data"
process(daily_url, daily_file)
print "Processing monthly data"
process(monthly_url, monthly_file)
